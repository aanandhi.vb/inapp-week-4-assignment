# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 27, 2020
# Description : Perform SQL Operations on database having following tables: Unique_Teams, Matches, Teams, Teams_in_Matches
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%203/question_3.py

import os
import sqlite3
from prettytable import PrettyTable

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()

print("\n---------- Part - A ----------\n")
table = PrettyTable()
a = (cursor.execute("SELECT HomeTeam, FTHG, FTAG FROM Matches WHERE Season=2010 AND HomeTeam='Aachen' ORDER BY FTHG DESC")).fetchall()
table.field_names = ['Home Team', 'FTHG', 'FTAG']
for row in a:
    table.add_row([row['HomeTeam'], row['FTHG'], row['FTAG']])
print(table)

print("\n---------- Part - B ----------\n")
table = PrettyTable()
b = (cursor.execute("SELECT HomeTeam, COUNT(FTR) FROM Matches WHERE FTR='H' AND Season=2016 GROUP BY HomeTeam ORDER BY COUNT(FTR) DESC")).fetchall()
table.field_names = ['Home Team','Total Games in 2016']
for row in b:
    table.add_row([row['HomeTeam'], row['COUNT(FTR)']])
print(table)


print("\n---------- Part - C ----------\n")
table = PrettyTable()
c = (cursor.execute("SELECT * FROM Unique_Teams LIMIT 10")).fetchall()
table.field_names = ['Team','TeamID']
for row in c:
    table.add_row([row['TeamName'], row['Unique_Team_ID']])
print(table)


print("\n---------- Part - D ----------\n")
table1 = PrettyTable()
d_where = (cursor.execute("SELECT * FROM Teams_in_Matches, Unique_Teams WHERE Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID")).fetchall()
d_join = (cursor.execute("SELECT * FROM Teams_in_Matches JOIN Unique_Teams ON Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID")).fetchall()
table1.field_names = ['MatchID','Unique TeamID', 'Team']
for row in d_where:
    table1.add_row([row['Match_ID'], row['Unique_Team_ID'], row['TeamName']])
print(table1)
print("\n\n\n")
table2 = PrettyTable()
table2.field_names = ['MatchID','Unique TeamID', 'Team']
for row in d_join:
    table2.add_row([row['Match_ID'], row['Unique_Team_ID'], row['TeamName']])
print(table2)


print("\n---------- Part - E ----------\n")
table = PrettyTable()
e = (cursor.execute("SELECT * FROM Unique_Teams JOIN Teams ON Unique_Teams.TeamName = Teams.TeamName LIMIT 10")).fetchall()
table.field_names = ['Team Name','Unique TeamID', 'Season']
for row in e:
    table.add_row([row['TeamName'], row['Unique_Team_ID'], row['Season']])
print(table)


print("\n---------- Part - F ----------\n")
table = PrettyTable()
f = (cursor.execute("SELECT * FROM Unique_Teams JOIN Teams ON Unique_Teams.TeamName = Teams.TeamName LIMIT 5")).fetchall()
table.field_names = ['Team Name','Unique TeamID', 'Season', 'AvgAgeHome', 'ForeignPlayersHome']
for row in f:
    table.add_row([row['TeamName'], row['Unique_Team_ID'], row['Season'], row['AvgAgeHome'], row['ForeignPlayersHome']])
print(table)

print("\n---------- Part - G ----------\n")
table = PrettyTable()
g = (cursor.execute("SELECT MAX(Match_ID), Teams_in_Matches.Unique_Team_ID, TeamName FROM Teams_in_Matches JOIN Unique_Teams ON Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID WHERE (TeamName LIKE '%y') OR (TeamName LIKE '%r') GROUP BY Teams_in_Matches.Unique_Team_ID, TeamName")).fetchall()
table.field_names = ['Unique TeamID', 'Team Name', 'MatchID (Max)']
for row in g:
    table.add_row([row['Unique_Team_ID'], row['TeamName'], row['MAX(Match_ID)']])
print(table)