# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 27, 2020
# Description : Perform SQL Operations on database having following tables: Unique_Teams, Matches, Teams, Teams_in_Matches
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%203/question_2.py

import os
import sqlite3
from prettytable import PrettyTable

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()

print("\n---------- Part - A ----------\n")
a = (cursor.execute("SELECT COUNT(*) FROM Teams")).fetchall()
print("No: of rows in Teams: ", a[0]["COUNT(*)"])

print("\n---------- Part - B ----------\n")
table = PrettyTable()
b = (cursor.execute("SELECT DISTINCT Season FROM Teams")).fetchall()
table.field_names = ['Season']
for row in b:
    table.add_row([row[0]])
print(table)

print("\n---------- Part - C ----------\n")
table = PrettyTable()
c = (cursor.execute("SELECT Max(StadiumCapacity), Min(StadiumCapacity) FROM Teams")).fetchall()
table.field_names = ['Stadium Capacity (Max)', 'Stadium Capacity (Min)']
for row in c:
    table.add_row([row['Max(StadiumCapacity)'], row['Min(StadiumCapacity)']])
print(table)

print("\n---------- Part - D ----------\n")
d = (cursor.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season=2014")).fetchall()
print("Sum of squad players in Teams: ", d[0]["SUM(KaderHome)"])

print("\n---------- Part - E ----------\n")
e = (cursor.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam='Man United'")).fetchall()
print("Average goals scored by Man United: ", round(e[0]["AVG(FTHG)"], 2))