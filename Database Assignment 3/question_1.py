# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 27, 2020
# Description : Perform SQL Operations on database having following tables: Unique_Teams, Matches, Teams, Teams_in_Matches
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%203/question_1.py

import os
import sqlite3
from prettytable import PrettyTable

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()

print("\n---------- Part - B ----------\n")
table = PrettyTable()
b = (cursor.execute("SELECT HomeTeam,AwayTeam FROM Matches WHERE Season=2015 AND FTHG=5")).fetchall()
table.field_names = ['Home Team', 'Away Team']
for row in b:
    table.add_row([row['HomeTeam'], row['AwayTeam']])
print(table)

print("\n---------- Part - C ----------\n")
table = PrettyTable()
c = (cursor.execute("SELECT * FROM Matches WHERE HomeTeam='Arsenal' AND FTR='A'")).fetchall()
table.field_names = ['Division', 'Season', 'Date', 'Home Team', 'Away Team', 'FTHG', 'FTAG', 'FTR']
for row in c:
    table.add_row([row['Div'], row['Season'], row['Date'], row['HomeTeam'], row['AwayTeam'], row['FTHG'], row['FTAG'], row['FTR']])
print(table)

print("\n---------- Part - D ----------\n")
table = PrettyTable()
d = (cursor.execute("SELECT * FROM Matches WHERE (Season BETWEEN 2012 AND 2015) AND AwayTeam='Bayern Munich' AND FTHG>2")).fetchall()
table.field_names = ['Division', 'Season', 'Date', 'Home Team', 'Away Team', 'FTHG', 'FTAG', 'FTR']
for row in d:
    table.add_row([row['Div'], row['Season'], row['Date'], row['HomeTeam'], row['AwayTeam'], row['FTHG'], row['FTAG'], row['FTR']])
print(table)

print("\n---------- Part - E ----------\n")
table = PrettyTable()
e = (cursor.execute("SELECT * FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%'")).fetchall()
table.field_names = ['Division', 'Season', 'Date', 'Home Team', 'Away Team', 'FTHG', 'FTAG', 'FTR']
for row in e:
    table.add_row([row['Div'], row['Season'], row['Date'], row['HomeTeam'], row['AwayTeam'], row['FTHG'], row['FTAG'], row['FTR']])
print(table)
