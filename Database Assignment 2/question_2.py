# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 24, 2020
# Description : To create table Department and perform SQL Operations on it
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%202/question_2.py

import os
import sqlite3

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()

department_data = [("D_100", "Marketing"), ("D_102", "Design"), ("D_104", "Development"), ("D_106", "Support"), ("D_108", "Maintenance")]
cursor.executemany("INSERT INTO Department (Department_Id, Department_Name) VALUES (?, ?)", department_data)

while(True):

    print("\n------- EMPLOYEE RECORDS -------\n")
    print('''1. Get Employees based on Department\n2. Exit''')
    user_choice = int(input("\nEnter your choice: "))
    if user_choice == 1:

        department_id = input("\nEnter Department ID: ")
        result = (cursor.execute("SELECT *, Department.Department_Name FROM Employee INNER JOIN Department ON Employee.Department_Id = Department.Department_Id WHERE Department.Department_Id=?", (department_id,))).fetchall()

        print(f"\n------- Employee Summary -------\n")
        for row in result:
            print(f"{row['Employee_Id']}. Name: {row['Employee_Name']}, Department ID: {row['Department_ID']}, Department Name: {row['Department_Name']}, Salary: {row['Salary']}")

    else:
        break


connection.commit()