# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 24, 2020
# Description : To create table Employee and perform SQL Operations on it
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%202/question_1.py

import os
import sqlite3

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

def print_employees():
    employee = (cursor.execute("SELECT * FROM Employee")).fetchall()
    print(f"\n------- Employee Summary -------\n")
    for row in employee:
        print(f"{row['Employee_Id']}. Name: {row['Employee_Name']}, Salary: {row['Salary']}")

def chosen_employee(result):
    print(f"\n------- Chosen Employee Summary -------\n")
    for row in result:
        print(f"{row['Employee_Id']}. Name: {row['Employee_Name']}, Department: {row['Department_ID']}, Salary: {row['Salary']}")


connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()
cursor.execute("""
    CREATE TABLE Department (
     Department_Id VARCHAR(50) NOT NULL,
     Department_Name VARCHAR(100) NOT NULL)""")
cursor.execute("""
    CREATE TABLE Employee (
     Employee_Id INTEGER PRIMARY KEY AUTOINCREMENT,
     Employee_Name VARCHAR(100) NOT NULL,
     Department_Id VARCHAR(50) NOT NULL,
     Salary INTEGER NOT NULL,
     FOREIGN KEY (Department_Id) REFERENCES Department (Department_Id))""")
cursor.execute("ALTER TABLE Employee ADD COLUMN City VARCHAR(100)")

employee_data = [("James", "D_100", 20000, "Barcelona"), ("Michael", "D_100", 95000, "Osaka"), ("John", "D_104", 30000, "Agra"), ("Susan", "D_106", 45000, "Los Angeles"), ("Sophia", "D_108", 70000, "Shanghai")]
cursor.executemany("INSERT INTO Employee (Employee_Name, Department_Id, Salary, City) VALUES (?, ?, ?, ?)", employee_data)

print_employees()

while(True):

    print("\n------- EMPLOYEE RECORDS -------\n")
    print('''1. Get Employees based on Name\n2. Get Employees based on Employee ID\n3. Change Employee Name\n4. View All Employees\n5. Exit''')
    user_choice = int(input("\nEnter your choice: "))
    if user_choice == 1:
        start_letter = input("\nEnter starting letter of employee name: ")
        result = (cursor.execute("SELECT * FROM Employee WHERE Employee_Name LIKE ? || '%'",(start_letter,))).fetchall()
        chosen_employee(result)

    elif user_choice == 2:
        employee_id = input("\nEnter Employee ID: ")
        result = (cursor.execute("SELECT * FROM Employee WHERE Employee_Id=?", (employee_id))).fetchall()
        chosen_employee(result)

    elif user_choice == 3:
        employee_id = input("\nEnter Employee ID: ")
        employee_name = input("\nEnter new Employee Name: ")
        cursor.execute("UPDATE Employee SET Employee_Name=? WHERE Employee_ID=?", (employee_name,employee_id))
        result = cursor.execute("SELECT * FROM Employee WHERE Employee_ID=?", (employee_id))
        chosen_employee(result)

    elif user_choice == 4:
        print_employees()

    else:
        break


connection.commit()