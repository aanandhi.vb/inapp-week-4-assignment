# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 23, 2020
# Description : To create tables Hospital and Doctor and perform SQL Operations on it
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%201/question_2.py


import os
import sqlite3

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()

cursor.execute("""
    CREATE TABLE Hospital (
     Hospital_Id INTEGER PRIMARY KEY AUTOINCREMENT,
     Hospital_Name VARCHAR(100) NOT NULL,
     Bed_Count INTEGER NOT NULL)""")

cursor.execute("""
    CREATE TABLE Doctor (
     Doctor_Id INTEGER PRIMARY KEY AUTOINCREMENT,
     Doctor_Name VARCHAR(100) NOT NULL,
     Hospital_Id INTEGER NOT NULL,
     Joining_Date VARCHAR(100) NOT NULL,
     Speciality VARCHAR(100) NOT NULL,
     Salary INTEGER NOT NULL,
     Experience VARCHAR(100),
     FOREIGN KEY (Hospital_Id) REFERENCES Hospital (Hospital_Id))""")
cursor.execute("INSERT INTO Doctor (Doctor_Id, Doctor_Name, Hospital_Id, Joining_Date, Speciality, Salary) VALUES (?, ?, ?, ?, ?, ?)", (100,'test',1,'2010-07-07',"Test",50000))
cursor.execute("DELETE FROM Doctor")

hospital_data = [("Mayo Clinic", 200), ("Cleveland Clinic", 400), ("Johns Hopkins Clinic", 1000), ("UCLA Medical Center", 1500)]
cursor.executemany("INSERT INTO Hospital (Hospital_Name, Bed_Count) VALUES (?, ?)", hospital_data)

doctor_data = [("David", 1, "2005-02-10", "Pediatric", 40000), ("Michael", 1, "2018-07-23", "Oncologist", 20000), ("Susan", 2, "2016-05-19", "Gynaecologist", 25000), ("Robert", 2, "2017-12-28", "Pediatric", 28000), ("Linda", 3, "2004-06-04", "Gynaecologist", 42000), ("William", 3, "2012-09-11", "Dermatologist", 30000), ("Richard", 4, "2014-08-21", "Gynaecologist", 32000), ("Karen", 4, "2011-10-17", "Radiologist", 30000)]
cursor.executemany("INSERT INTO Doctor (Doctor_Name, Hospital_Id, Joining_Date, Speciality, Salary) VALUES (?, ?, ?, ?, ?)", doctor_data)

while(True):

    print("\n---- HOSPITAL RECORDS ----\n")
    print('''1. Get Doctors based on Speciality and Salary\n2. Get Doctors based on Hospital ID\n3. Exit''')

    user_choice = int(input("\nEnter your choice: "))
    if user_choice == 1:
        speciality = input("\nEnter speciality: ")
        salary = input("\nEnter salary: ")
        logic_choice = int(input("\nEnter above(0), below(1), equal(2): "))
        result = (cursor.execute("SELECT Doctor_Id, Doctor_Name, Speciality, Salary FROM Doctor WHERE Salary>? AND Speciality=?", (salary, speciality))).fetchall() if logic_choice == 0 else (cursor.execute("SELECT * FROM Doctor WHERE Salary<? AND Speciality=?", (salary, speciality))).fetchall() if logic_choice == 1 else (cursor.execute("SELECT * FROM Doctor WHERE Salary=? AND Speciality=?", (salary, speciality))).fetchall()
        print(f"\n------Doctor Summary-------\n")
        for row in result:
            print(f"{row['Doctor_Id']}. Name: {row['Doctor_Name']}, Speciality: {row['Speciality']}, Salary: {row['Salary']}")

    elif user_choice == 2:
        hospital_id = input("\nEnter hospital ID: ")
        result = (cursor.execute("SELECT *, Hospital.Hospital_Name FROM Doctor INNER JOIN Hospital ON Doctor.Hospital_Id = Hospital.Hospital_Id WHERE Hospital.Hospital_Id=?", (hospital_id))).fetchall()

        print(f"\n------Doctor Summary-------\n")
        for row in result:
            print(f"{row['Doctor_Id']}. Name: {row['Doctor_Name']}, Speciality: {row['Speciality']}, Salary: {row['Salary']}, Hospital: {row['Hospital_Name']}")

    else:
        break

connection.commit()
