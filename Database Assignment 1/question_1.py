# InApp Week - 4 Assignment
# Author      : Aanandhi V B
# Date        : November 23, 2020
# Description : To create a table Cars with two columns : Name, Owner and perform SQL Operations on it
# Project URL : https://gitlab.com/aanandhi.vb/inapp-week-4-assignment/-/blob/master/Database%20Assignment%201/question_1.py

import os
import sqlite3

DEFAULT_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')

def db_connect(db_path=DEFAULT_PATH):
    con = sqlite3.connect(db_path)
    return con

connection = db_connect()
connection.row_factory = sqlite3.Row
cursor = connection.cursor()
cursor.execute("""
    CREATE TABLE Cars (
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     name VARCHAR(100) NOT NULL,
     owner VARCHAR(100) NOT NULL)""")

car_values = "INSERT INTO Cars (name, owner) VALUES (?, ?)"
counter = 1
while(counter <= 10):
    print("\n-----Car {}-----".format(counter))
    name = input("\nEnter car name: ")
    owner = input("\nEnter car owner: ")
    cursor.execute(car_values, (name, owner))
    counter += 1

result = (cursor.execute("SELECT * FROM Cars")).fetchall()
print(f"\n------Cars Summary-------\n")
for row in result:
    print(f"{row['id']}. Car: {row['name']}, Owner: {row['owner']}")

connection.commit()